package com.messaing.msgdemo.service;

import com.messaing.msgdemo.db.MessageTable;
import java.util.List;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.stereotype.Service;

@Service
public class MSGService {

  private MessageTable messageTable;

  public MSGService(MessageTable messageTable) {
    this.messageTable = messageTable;
  }

  public List<?> getMessages () {
    return messageTable.findAll();
  }

}
