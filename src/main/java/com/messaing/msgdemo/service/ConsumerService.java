package com.messaing.msgdemo.service;

import com.messaing.msgdemo.db.MessageTable;
import com.messaing.msgdemo.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Component;

@Component
public class ConsumerService implements RabbitListenerConfigurer {

  private static final Logger logger = LoggerFactory.getLogger(ConsumerService.class);

  MessageTable messageTable;

  public ConsumerService(MessageTable messageTable) {
    this.messageTable = messageTable;
  }

  @RabbitListener(queues = "${spring.rabbitmq.queue}")
  public void receiveMessage (Message message) {
    logger.info("Message Received:" + message.getBody());
    messageTable.save(message);
  }

  @Override
  public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {

  }
}
