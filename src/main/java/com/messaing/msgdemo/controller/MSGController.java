package com.messaing.msgdemo.controller;

import com.messaing.msgdemo.service.MSGService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/messages")
public class MSGController {

  MSGService msgService;

  public MSGController(MSGService msgService) {
    this.msgService = msgService;
  }

  @GetMapping
  public ResponseEntity<?> getMessages () {
    return ResponseEntity.ok(msgService.getMessages());
  }


}
